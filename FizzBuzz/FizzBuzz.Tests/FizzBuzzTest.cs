using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using FizzBuzz.Models;
using FizzBuzz.BussinessLogic;
using FizzBuzz.Repository;
using NUnit.Framework;
using CollectionAssert = NUnit.Framework.CollectionAssert;
using Assert = NUnit.Framework.Assert;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzTest
    {
        public FizzBuzzTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private readonly DateTime notWednesday = new DateTime(2022, 2, 10);
        private readonly DateTime wednesday = new DateTime(2022, 2, 9);
        FizzBuzzBL fizzBuzzBL = new FizzBuzzBL(new FizzBuzzDAL());
        FizzBuzzModel model = new FizzBuzzModel();
        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_If_Input_Is_One()
        {

            // Arrange
            var expected = new List<string> { "1" };

            // Act
            var actual = fizzBuzzBL.GetNumberList(1, notWednesday);

            // Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_Two_If_Input_Is_Two()
        {

            // Arrange

            var expected = new List<string> { "1", "2" };

            // Act
            var actual = fizzBuzzBL.GetNumberList(2, notWednesday);

            // Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_Two_Fizz_If_Input_Is_Three()
        {
            // Arrange
  
            var expected = new List<string> { "1", "2", "Fizz" };

            // Act
            var actual = fizzBuzzBL.GetNumberList(3, notWednesday);

            // Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_Two_Wizz_If_Input_Is_Three_And_Is_Wednesday()
        {
            // Arrange

            var expected = new List<string> { "1", "2", "Wizz" };

            // Act
            var actual = fizzBuzzBL.GetNumberList(3, wednesday);

            // Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_Two_Fizz_Four_Buzz_If_Input_Is_Five()
        {
            // Arrange
            var expected = new List<string> { "1", "2", "Fizz", "4", "Buzz" };

            // Act
            var actual = fizzBuzzBL.GetNumberList(5, notWednesday);

            // Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_Two_Wizz_Four_Wuzz_If_Input_Is_Five_And_Is_Wednesday()
        {
            // Arrange
            var expected = new List<string> { "1", "2", "Wizz", "4", "Wuzz" };

            // Act
            var actual = fizzBuzzBL.GetNumberList(5,  wednesday);

            // Assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GetFizzBuzz_Should_Return_FizzBuzz_For_15th_Position_If_Input_Is_15_Or_Greater()
        {
            // Arrange
            var position = 15;
            var expected = "FizzBuzz";

            // Act
            var actualFizzBuzzList = fizzBuzzBL.GetNumberList(position, notWednesday);
            var actual = actualFizzBuzzList.ElementAt(position - 1); // As lists are 0 index based we need to access the 14th item to get the correct position

            // Assert
            Assert.AreEqual(expected, actual);

        }


        [Test]
        public void GetFizzBuzz_Should_Return_WizzWuzz_For_15th_Position_If_Input_Is_15_Or_Greater_And_Is_Wednesday()
        {
            // Arrange
            var position = 15;
            var expected = "WizzWuzz";

            // Act
            var actualFizzBuzzList = fizzBuzzBL.GetNumberList(position, wednesday);
            var actual = actualFizzBuzzList.ElementAt(position - 1); // As lists are 0 index based we need to access the 14th item to get the correct position

            // Assert
            Assert.AreEqual(expected, actual);

        }


        [Test]
        public void GetFizzBuzz_Should_Return_Exception_If_Input_Is_Less_Than_One()
        {

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => fizzBuzzBL.GetNumberList(0, wednesday));

        }

        [Test]
        public void GetFizzBuzz_Should_Return_Exception_If_Input_Is_Greater_Than_Thousand()
        {
           

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => fizzBuzzBL.GetNumberList(10001, notWednesday));

        }

    }
}
