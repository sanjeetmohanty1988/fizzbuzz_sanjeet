﻿using FizzBuzz.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using FizzBuzz.DAL;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        #region Private Varibales
        private IFizzBuzz _fizzBuzz;
        #endregion
        #region Constructor
        public FizzBuzzController(IFizzBuzz fizzBuzz)
        {
            _fizzBuzz = fizzBuzz;
        }
        #endregion

        [HttpGet]
        public IActionResult Index()
        {
            return View(new FizzBuzzModel());
        }
        [HttpPost]
        public IActionResult Index(FizzBuzzModel _model, int? _page)
        {
            int _maxRows = 20;
            int _pageNumber = (_page ?? 1);

            if (ModelState.IsValid)
            {
                DateTime _dt = DateTime.UtcNow;
                List<string> ListFizzBuzz = _fizzBuzz.GetNumberList(_model.userInptVal, _dt);
                _model.fizzBuzzNum = ListFizzBuzz.Skip((_pageNumber - 1) * _maxRows).Take(_maxRows).ToList();
                double _pageCount = (double)((decimal)_model.fizzBuzzNum.Count() / Convert.ToDecimal(_maxRows));
                _model.pageCount = (int)Math.Ceiling(_pageCount);
                _model.currentPageIndex = _pageNumber;
                return RedirectToAction("FizzBuzz", new { _value = _model.userInptVal, _page = 1});
            }
            else
                return View(_model);
        }

        [HttpGet]
        public IActionResult Fizzbuzz(int _value, int? _page)
        {
            int _maxRows = 20;
            int _pageNumber = (_page ?? 1);
            DateTime _dt = DateTime.UtcNow;
            List<string> ListFizzBuzz = _fizzBuzz.GetNumberList(_value, _dt);
            FizzBuzzModel _model = new FizzBuzzModel();
            _model.fizzBuzzNum = ListFizzBuzz.ToList();
            double _pageCount = (double)((decimal)_model.fizzBuzzNum.Count() / Convert.ToDecimal(_maxRows));
            _model.pageCount = (int)Math.Ceiling(_pageCount);
            _model.currentPageIndex = _pageNumber;
            _model.fizzBuzzNum= ListFizzBuzz.Skip((_pageNumber - 1) * _maxRows).Take(_maxRows).ToList();
            _model.userInptVal = _value;
            return View(_model);
        }
    }
}
