﻿using System;
using System.Collections.Generic;
using FizzBuzz.DAL;

namespace FizzBuzz.Repository
{
    public class FizzBuzzDAL:IFizzBuzz
    {
        public  List<string> GetNumberList(int _value,DateTime _dt)
        {
            if (_value < 1 || _value > 1000)
                throw new ArgumentException("Value should between 0 and 1000");

            var _numbers = new List<string>();

            for (var i = 1; i <= _value; i++)
            {
                _numbers.Add(GetFizzBuzzValue(i,_dt));
            }
            return _numbers;
        }
        private  string GetFizzBuzzValue(int i,DateTime _now)
        {
            if (i % 3 == 0 && i % 5 == 0)
            {
                if (_now.DayOfWeek == DayOfWeek.Wednesday)
                    return "WizzWuzz";
                else
                    return "FizzBuzz";
            }
            else if (i % 3 == 0)
            {
                if (_now.DayOfWeek == DayOfWeek.Wednesday)
                    return "Wizz";
                else
                    return "Fizz";
            }
            else if (i % 5 == 0)
            {
                if (_now.DayOfWeek == DayOfWeek.Wednesday)
                    return "Wuzz";
                else
                    return "Buzz";
            }
            else
            {
                return i.ToString();
            }
        }
    }
}
