﻿using System;
using System.Collections.Generic;

namespace FizzBuzz.DAL
{
    public interface IFizzBuzz
    {
        public List<string> GetNumberList(int _value, DateTime _dt);
    }
}
