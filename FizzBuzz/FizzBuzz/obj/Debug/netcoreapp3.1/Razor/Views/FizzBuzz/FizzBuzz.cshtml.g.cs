#pragma checksum "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e7d1b8624c07f3e11aa1d06053548c65e40fb88a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_FizzBuzz_FizzBuzz), @"mvc.1.0.view", @"/Views/FizzBuzz/FizzBuzz.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\_ViewImports.cshtml"
using FizzBuzz;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\_ViewImports.cshtml"
using FizzBuzz.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e7d1b8624c07f3e11aa1d06053548c65e40fb88a", @"/Views/FizzBuzz/FizzBuzz.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"025be84243bf66e434f933bab197463da7479f47", @"/Views/_ViewImports.cshtml")]
    public class Views_FizzBuzz_FizzBuzz : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<FizzBuzz.Models.FizzBuzzModel>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
            WriteLiteral("\n\n<div>\n    <h3>FizzBuzz</h3>\n</div>\n\n\r\n<!DOCTYPE html>\n<html>\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e7d1b8624c07f3e11aa1d06053548c65e40fb88a3400", async() => {
                WriteLiteral(@"
    <title>FizzBuzz</title>
    <meta name=""viewport"" content=""width=device-width"" />
    <title>Index</title>
    <style type=""text/css"">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            background-color: #fff;
        }

            table th {
                background-color: #B8DBFD;
                color: #333;
                font-weight: bold;
            }

            table th, table td {
                padding: 5px;
                border: 1px solid #ccc;
            }

            table, table table td {
                border: 0px solid #ccc;
            }
    </style>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e7d1b8624c07f3e11aa1d06053548c65e40fb88a5098", async() => {
                WriteLiteral("\n");
#nullable restore
#line 45 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
     using (Html.BeginForm("Index", "FizzBuzz", FormMethod.Post))
    {

#line default
#line hidden
#nullable disable
                WriteLiteral("    <div class=\"row\">\n        <div class=\"col-md-4 col-md-offset-4\">\n\n");
#nullable restore
#line 50 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
             foreach (var number in Model.fizzBuzzNum)
            {
            if (number == "FizzBuzz")
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("            <span class=\"fizz\">Fizz</span><span class=\"buzz\">Buzz</span> ");
#nullable restore
#line 54 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                                                                         }
            else
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("            <span");
                BeginWriteAttribute("class", " class=\"", 1239, "\"", 1264, 1);
#nullable restore
#line 57 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
WriteAttributeValue("", 1247, number.ToLower(), 1247, 17, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("> ");
#nullable restore
#line 57 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                                        Write(number);

#line default
#line hidden
#nullable disable
                WriteLiteral("</span> ");
#nullable restore
#line 57 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                                                            }

#line default
#line hidden
#nullable disable
                WriteLiteral("            <br />");
#nullable restore
#line 58 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                  }

#line default
#line hidden
#nullable disable
                WriteLiteral("\n        </div>\n    </div>\n    <br />\n    <table cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n");
#nullable restore
#line 65 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
             for (int i = 1; i <= Model.pageCount; i++)
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("            <td>\n");
#nullable restore
#line 68 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                 if (i != Model.currentPageIndex)
                {

#line default
#line hidden
#nullable disable
                WriteLiteral("                <a");
                BeginWriteAttribute("href", " href=\"", 1572, "\"", 1624, 5);
                WriteAttributeValue("", 1579, "javascript:PagerClick(", 1579, 22, true);
#nullable restore
#line 70 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
WriteAttributeValue("", 1601, i, 1601, 2, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 1603, ",", 1603, 1, true);
#nullable restore
#line 70 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
WriteAttributeValue("", 1604, Model.userInptVal, 1604, 18, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 1622, ");", 1622, 2, true);
                EndWriteAttribute();
                WriteLiteral(">");
#nullable restore
#line 70 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                                                                   Write(i);

#line default
#line hidden
#nullable disable
                WriteLiteral("</a>\n");
#nullable restore
#line 71 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
                WriteLiteral("                <span>");
#nullable restore
#line 74 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                 Write(i);

#line default
#line hidden
#nullable disable
                WriteLiteral("</span>\n");
#nullable restore
#line 75 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
                }

#line default
#line hidden
#nullable disable
                WriteLiteral("            </td>\n");
#nullable restore
#line 77 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
            }

#line default
#line hidden
#nullable disable
                WriteLiteral("        </tr>\n    </table>\n");
                WriteLiteral("    <input type=\"hidden\" id=\"hfCurrentPageIndex\" name=\"currentPageIndex\" />\n");
#nullable restore
#line 82 "C:\Users\DELL\OneDrive\Desktop\FizzBuzz_Sanjeet\FizzBuzz\FizzBuzz\Views\FizzBuzz\FizzBuzz.cshtml"
    }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"    <script type=""text/javascript"">
        function PagerClick(index, inputVal) {
            document.getElementById(""hfCurrentPageIndex"").value = index;
            window.location.href = ""../FizzBuzz/FizzBuzz?_value="" + inputVal + ""&_page=""+ index ;
        }
    </script>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n</html>\n\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FizzBuzz.Models.FizzBuzzModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
