﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace FizzBuzz.Models
{
    public class FizzBuzzModel
    {
        ///<summary>
        /// Gets or sets User Input value.
        ///</summary>
        ///
        [Range(1, 1000)]
        [Required]
        [DisplayName("Value")]
        public int userInptVal { get; set; }
        ///<summary>
        /// Gets or sets fizzBuzz Number.
        ///</summary>
        public List<String> fizzBuzzNum { get; set; }

        ///<summary>
        /// Gets or sets CurrentPageIndex.
        ///</summary>
        public int currentPageIndex { get; set; }

        ///<summary>
        /// Gets or sets PageCount.
        ///</summary>
        public int pageCount { get; set; }
    }
}
