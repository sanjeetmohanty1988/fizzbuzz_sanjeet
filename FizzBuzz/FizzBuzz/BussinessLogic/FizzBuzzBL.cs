﻿using System;
using System.Collections.Generic;
using FizzBuzz.DAL;

namespace FizzBuzz.BussinessLogic
{
    public class FizzBuzzBL
    {
        public IFizzBuzz _fizzbuzzDAL;
        public FizzBuzzBL(IFizzBuzz _fizzbuzzDAL)
        {
            this._fizzbuzzDAL = _fizzbuzzDAL;
        }
        public  List<string> GetNumberList(int _value,DateTime _dt)
        {
            return _fizzbuzzDAL.GetNumberList(_value,_dt);
        }
    }
}
